# TP final - IAI UNSAM 2021

La entrega está en `output/renders`, generados a partir de los notebooks:

* `01-EDA.Rmd`
* `02-Clasificacion.Rmd`
* `03-Ensembles.Rmd` (boosted GLM de yapa).

# Correcciones

El conjunto de test se usa solo con el modelo final, cuanto ya fue elegido en base al conjunto de train.
Usarlo para comprar métodos es un error.

Poner descripciones de variables.

Las figuras de fotos no se entendian: no quedaba claro que era cada una (Pos vs Neg).
