---
title: "ML UNSAM 2021 - TP Final: Ensemble methods"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_float:
      collapsed: false
    toc_depth: 3
    number_sections: false
    smooth_scroll: false
    code_folding: hide
  # pdf_document:
  #   latex_engine: xelatex
  #   toc: true
  #   toc_depth: 2
  #   number_sections: true
editor_options:
  chunk_output_type: inline
author: NM
date: "`r format(Sys.time(), '%d %B, %Y')`"
urlcolor: blue
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding, output_dir = "output/renders") })
---

```{r setup, message=F}
knitr::opts_chunk$set(message = F)
library(utiles)
library(tidyverse)
library(magick)
library(rcell2)
```

## Propuesta

Ya que el GLM regularizado estuvo bien, pero no _tan_ bien, quizas valga boostearlo.

¿Funcionaría?

## Cargar datos

```{r}
cell.data <- rcell2::cell.load.alt("data/yeasts/")

cdata.full <- cell.data$data %>%
  mutate(cf.m1.tfp = (f.tot.m1.tfp - a.tot.m1 * f.bg.tfp) / a.tot.m1)

pdata <- data.frame(pos = 3)

images <- cell.data$images

cdata <- readRDS("data/cdata.classi.RDS") # solo usé el t.frame==0 para clasificar
```

### Train test split

En R se puede usar `caret` para hacer CV con tu método favorito.

```{r}
library(caret)
```

```{r}
# Split the data into training and test set
set.seed(123)
training.samples <- createDataPartition(cdata$clase.factor, p = 0.80, list = F)

train_data <- cdata[training.samples[,1], ]
test_data <- cdata[-training.samples[,1], ]
```

```{r}
train_data$clase.factor %>% table() %>% addmargins()
test_data$clase.factor %>% table() %>% addmargins() %>% signif(2)
```

```{r}
train_data$clase.factor %>% {table(.)/length(.)} %>% addmargins() %>% signif(2)
test_data$clase.factor %>% {table(.)/length(.)} %>% addmargins() %>% signif(2)
```

## Boosting I: GLM Logistic regression

Un GLM con link logit, validación cruzada y regularización:

* https://towardsdatascience.com/the-5-classification-evaluation-metrics-you-must-know-aa97784ff226
* https://stackoverflow.com/a/43974934
* http://rstudio-pubs-static.s3.amazonaws.com/42780_3d4d218ce86241eb9855222b82bbde44.html

Also:

> Unlike the glm function, glmboost will perform variable selection. After fitting the model, score the test data set and measure the AUC.

Usé las 4 variables de antes pero, ya que el método hace selección, podría haber usado todas. 

### Fit

* Repeated cross-calidation: https://discuss.analyticsvidhya.com/t/what-is-repeated-cv-in-caret/12222
* `mboost` library: https://rpubs.com/crossxwill/mboost

```{r}
fitControl = trainControl(method = "repeatedcv",
                          number = 5, repeats = 5, 
                          summaryFunction=mnLogLoss,  # para categorical cross-entropy
                          # summaryFunction=twoClassSummary,  # para ROC
                          search = "random",  # para "tuneLength"
                          # savePredictions = T,
                          classProbs = TRUE
                          )
```

```{r}
# set.seed(2014)
# 
# no_cores <- parallel::detectCores() - 1
# cl <- parallel::makeCluster(no_cores,
#                             setup_strategy = "sequential",
#                             outfile = "/tmp/dopar.txt")
# doParallel::registerDoParallel(cl)
# 
# mod <- train(clase.factor ~ a.tot + fft.stat + cf.m1.tfp + el.p, 
#              data = train_data, 
#              
#              preProcess = c('center', 'scale'),
#              # center = T,
#              
#              method = "glmboost", 
#              
#              metric="logLoss",
#              # metric="ROC",
#              
#              trControl = fitControl,
#              
#              tuneLength=25, 
#              
#              family=mboost::Binomial(link = c("logit")))
# 
# parallel::stopCluster(cl)

# saveRDS(mod, "data/models/mod.boostglm_4vars.RDS")
mod <- readRDS("data/models/mod.boostglm_4vars.RDS")

modelo <- mod$finalModel
```

> Mil años estuvo fitteando...

### Revisión

Regularizacion e importancia de variables:

* varImp para GLMs: "*the absolute value of the t-statistic for each model parameter is used.*" (escalados a 100%).

```{r}
caret::varImp(mod)$importance %>% arrange(-Overall)
```

Curiosamente decidió que `a.tot` no es importante.

```{r}
mod$bestTune
```

Usó las 1000 iteraciones que tenía.

```{r}
plot(mod)
```

Predecir clases:

* https://stats.stackexchange.com/a/77549/295029

```{r}
train_data$clase.factor.predict <- predict(mod)
```

Columna indicadora de predicciones correctas o incorrectas:

```{r}
train_data$clase.pred.good <- with(train_data, clase.factor == clase.factor.predict)
```

Predecir probabilidades de clase:

```{r}
predict.probas <- predict(mod, type = "prob")

train_data[,names(predict.probas)] <- predict.probas

train_data$max.proba <- apply(train_data[,names(predict.probas)], 1, max)
```

#### Distribucion de probas

Distribucion de probas por clase y acierto/error:

```{r}
train_data %>% 
  group_by(ucid) %>% mutate(max_proba = max(Neg, Pos)) %>% ungroup() %>% 
  
  select(clase.factor, clase.pred.good, max_proba) %>% 
  pivot_longer(cols = max_proba, names_to = "clase", values_to = "probabilidad") %>% 
  
  ggplot() +
    geom_histogram(aes(x=probabilidad, color=clase.pred.good), position = "identity",
                 # bins = 10,
                 breaks = seq(0,1,length.out=12),
                 fill = NA) +
    # facet_grid(clase.pred.good~clase.factor, scales = "free_y") +
    facet_wrap(~clase.factor, scales = "free_y") +
  theme_minimal()
```

Definitivamente no dio como el SVM.

Pero la distrubucion dio mejor que la del GLM.

#### Matrices de confusion

```{r}
# with(train_data, table(clase.factor)) %>% addmargins()
# with(train_data, table(clase.factor, clase.factor.predict)) %>% addmargins()
with(train_data, 
     caret::confusionMatrix(clase.factor, 
                            clase.factor.predict))
```

Pero, en definitiva, no dio mal.

#### ROC y PR

```{r}
umbrales <- seq(0,1,length.out=1000) %>% as.matrix()

# Para cada umbral de probabilidad
roc <- sapply(umbrales, function(umbral=0.96){
    
    # Calcular clases
    clase.predict.umbral <- case_when(
      predict.probas$Pos >= umbral ~ "Pos",
      predict.probas$Pos < umbral ~ "Neg",
      TRUE ~ NA_character_
    ) %>% factor(levels = levels(train_data$clase.factor))
  
    # Calcular métricas
    cm.umbral <- 
      caret::confusionMatrix(data = clase.predict.umbral,
                             reference = train_data$clase.factor,
                             positive = "Pos")
    
    # Devolver métricas junto al umbral
    return(c(umbral=umbral, cm.umbral$byClass))
    
  }, simplify = TRUE, USE.NAMES = TRUE) %>% 
  # Convertir en dataframe lindo
  t() %>% as.data.frame()
```


```{r}
plt.roc <- 
  roc %>% arrange(umbral) %>% 
    ggplot(aes(x=1-Specificity,y=Sensitivity, color=umbral)) + geom_point() +
    geom_abline(slope=1, intercept = 0) + 
    coord_equal() + xlim(c(0,1)) + ylim(c(0,1)) +
    ggtitle("ROC")

# plotly::ggplotly(plt.roc)
plt.roc
```

```{r}
roc %>% arrange(umbral) %>% 
  ggplot(aes(y=Precision,x=Recall, color=umbral)) + geom_path() +
  ggtitle("PR")
```

En general dio casi igual o un poco peor que el GLM solo.

#### Revisar imagenes

Revisar imagenes de celulas mal clasificadas:

```{r}
pics <- 
  train_data %>% filter(!clase.pred.good) %>% 
  split(.$clase.factor) %>% 
    lapply(function(d){
      magickCell(d, paths = images, n.cells = 9,
                 ch = c("tfp", "tfp.out"), equalize_images = T) %>%
        image_annotate(text = d$clase.factor[1], boxcolor = "purple", gravity = "northeast") %>% 
        image_frame(color = "white") %>% 
        magickForKnitr(.resize = "x500")
    }) %>% unlist()

knitr::include_graphics(pics)
```

Es la misma historia de antes, tiene problemas con las `Pos` que están gemando,
y con las `Neg` muertas (y algunas otras desenfocadas).

#### Graficar fronteras

Armo un grid y predigo para cada punto:

```{r}
# new.data <- train_data %>% 
#   select(a.tot, fft.stat, cf.m1.tfp, el.p) %>% 
#   lapply(function(d){
#     d.range <- range(d)
#     seq(from=d.range[1]*0.9, to=d.range[2]*1.1, length.out=30)
#   }) %>% 
#   as.data.frame()
# 
# new.data <- tidyr::expand(data = new.data,
#                           a.tot, fft.stat, cf.m1.tfp, el.p)
# 
# new.predict.probas <- predict(mod, newdata = new.data, type = "prob")
# 
# new.data[,names(predict.probas)] <- new.predict.probas
# 
# new.data$max.prob <- apply(new.predict.probas, 1, max)
# new.data$clase.factor <-  predict(mod, newdata = new.data)

new.data <- readRDS("data/grid.RDS")
```

Regiones donde todas las probas de una clase superan 0.99:

```{r}
new.data %>% 
  mutate(a.tot.bin = cut(a.tot, breaks=30+1, ordered_result=T),
         fft.stat.bin = cut(fft.stat, breaks=30+1, ordered_result=T)) %>%
  group_by(a.tot.bin, fft.stat.bin) %>%
  summarise(
    class.99 = case_when(
      all(Pos > 0.99) ~ "Pos",
      all(Neg > 0.99) ~ "Neg",
      T~NA_character_
    ),
    a.tot = a.tot[1],
    fft.stat = fft.stat[1]
    ) %>%
ggplot() +
  geom_tile(aes(x=a.tot, y=fft.stat, fill=class.99)) +
  geom_point(aes(x=a.tot,y=fft.stat, color=clase.factor, shape=clase.pred.good), data = train_data) +
  scale_fill_viridis_d() +
  ggtitle("Regiones donde todas las probas de una clase superan 0.99")
```

Promedio de probabilidad de la clase positiva `Pos`:

> La idea es hacer la version continua del grafico anterior.

```{r}
new.data %>% 
  mutate(a.tot.bin = cut(a.tot, breaks=30+1, ordered_result=T),
         fft.stat.bin = cut(fft.stat, breaks=30+1, ordered_result=T)) %>%
  group_by(a.tot.bin, fft.stat.bin) %>%
  summarise(
    mean.pos.proba = mean(Pos),
    a.tot = a.tot[1],
    fft.stat = fft.stat[1]
    ) %>%
  ggplot() +
    geom_tile(aes(x=a.tot, y=fft.stat, fill=mean.pos.proba)) +
    geom_point(aes(x=a.tot,y=fft.stat, color=clase.factor, shape=clase.pred.good), data = train_data) +
    scale_fill_viridis_c() +
    scale_color_manual(values = c("white", "black")) +
    ggtitle("Promedio de probabilidad de la clase positiva (Pos)")

```

Intento graficar fronteras fijando valores de las otras dos variables.

Armo un grid con dos variables fijas, y predigo para cada punto:

```{r}
# new.data <- train_data %>% 
#   # select(a.tot, fft.stat, cf.m1.tfp, el.p) %>% 
#   select(a.tot, fft.stat) %>%
#   lapply(function(d) seq(from=range(d)[1]*0.9, to=range(d)[2]*1.1, length.out=50)) %>% 
#   as.data.frame() %>% 
#   mutate(cf.m1.tfp=1500, el.p=0.975)
# 
# new.data <- tidyr::expand(data = new.data,
#                           a.tot, fft.stat, cf.m1.tfp, el.p)
# 
# new.predict.probas <- predict(mod, newdata = new.data, type = "prob")
# 
# new.data[,names(predict.probas)] <- new.predict.probas
# 
# new.data$max.prob <- apply(new.predict.probas, 1, max)
# new.data$clase.factor <-  predict(mod, newdata = new.data)

new.data <- readRDS("data/grid2.RDS")
```

Se ve la frontera para un par de valores fijos de `cf.m1.tfp` y `el.p`:

```{r}
ggplot(new.data) +
  geom_tile(aes(x=a.tot, y=fft.stat, fill=Pos>Neg)) +
  geom_point(aes(x=a.tot,y=fft.stat, color=clase.factor, shape=clase.pred.good), data = train_data) +
  scale_fill_viridis_d() +
  ggtitle("Una frontera de decision", "fijando valores de las otras dos variables")
```

### Test

```{r}
test_data$clase.factor.predict <- predict(mod, newdata = test_data)

predict.probas <- predict(mod, type = "prob", newdata = test_data)

test_data[,names(predict.probas)] <- predict.probas

test_data$max.proba <- apply(test_data[,names(predict.probas)], 1, max)

test_data$clase.pred.good <- with(test_data, clase.factor == clase.factor.predict)
```

A este le fue mejor en test que en train:

```{r}
with(test_data, 
     caret::confusionMatrix(clase.factor, 
                            clase.factor.predict))
```

La matriz de confusion es igual a la del GLM con 4 variables.

Pero la distribucion de probabilidades cambió, corriéndose a la derecha:

```{r}
test_data %>% 
  group_by(ucid) %>% mutate(max_proba = max(Neg, Pos)) %>% ungroup() %>% 
  
  select(clase.factor, clase.pred.good, max_proba) %>% 
  pivot_longer(cols = max_proba, names_to = "clase", values_to = "probabilidad") %>% 
  
  ggplot() +
    geom_histogram(aes(x=probabilidad, color=clase.pred.good), position = "identity",
                 # bins = 10,
                 breaks = seq(0,1,length.out=12),
                 fill = NA) +
    # facet_grid(clase.pred.good~clase.factor, scales = "free_y") +
    facet_wrap(~clase.factor, scales = "free_y") +
  theme_minimal()
```

Revisar imagenes de celulas mal clasificadas:

```{r}
pics <- 
  test_data %>% filter(!clase.pred.good) %>% 
  split(.$clase.factor) %>% 
    lapply(function(d){
      magickCell(d, paths = images, n.cells = 9,
                 ch = c("tfp", "tfp.out"), equalize_images = T) %>%
        image_annotate(text = d$clase.factor[1], boxcolor = "purple", gravity = "northeast") %>% 
        image_frame(color = "white") %>% 
        magickForKnitr(.resize = "x500") 
    }) %>% unlist()

knitr::include_graphics(pics)
```

## Bagging I: RF

### To do

## Bagging II: SVM

### To do
